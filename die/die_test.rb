require 'minitest/autorun'
require 'minitest/pride'

require 'die'

describe Die do
  it "has sides" do
    Die.instance_methods.include?(:sides).must_equal true
  end

  describe "#initialize" do
    it "is initialized with sides" do
      Die.new(4).sides.must_equal 4
    end

    it "defaults to 6" do
      Die.new.sides.must_equal 6
    end

    it "wont initialize with less than 4 sides" do
      -> {
        Die.new(3)
      }.must_raise NotEnoughSidesError
    end

    it "wont initialize with an odd number of sides" do
      -> {
        Die.new(5)
      }.must_raise OddNumberSidesError
    end
  end

  describe "#roll" do
    it "is less than or equal to the #sides" do
      Die.new(4).roll.must_be :<=, 4
    end

    it "is greater than 0" do
      Die.new(4).roll.must_be :>, 0
    end
  end
end
