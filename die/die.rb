NotEnoughSidesError = RuntimeError
OddNumberSidesError = RuntimeError

class Die
  attr_reader :sides

  def initialize(sides=6)
    raise NotEnoughSidesError unless sides >= 4
    raise OddNumberSidesError unless sides.even?
    @sides = sides
  end

  def roll
    rand(1..sides)
  end
end
