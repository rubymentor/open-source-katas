require 'prime'

module Proper
  refine Integer do
    def proper_divisors
      return [] if self == 1
      primes = prime_division.flat_map{ |prime, freq| [prime] * freq }
      # by it's nature in Ruby, each_with_object/reduce are side effecting
      # methods... We will live with this little bit o mutability
      (1...primes.size).each_with_object([1]) do |n, r|
        primes.combination(n).map{|comb| r << comb.inject(:*)}
      end.flatten.uniq
    end
  end
end

